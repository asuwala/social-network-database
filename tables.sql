CREATE DATABASE SocialNetwork2;
GO
USE SocialNetwork2;
GO
CREATE SCHEMA socnet;
GO
-------------------- TABLES --------------------
CREATE TABLE socnet.Users (
	id INT IDENTITY(1,1) NOT NULL,
	firstName NVARCHAR(100) NOT NULL,
	lastName NVARCHAR(100) NOT NULL,
	email NVARCHAR(200) NOT NULL,
	registrationTime DATETIME NOT NULL,
	password NVARCHAR(256) NOT NULL,
)
GO
ALTER TABLE socnet.Users ADD CONSTRAINT PK_Users PRIMARY KEY CLUSTERED (id);

GO

CREATE TABLE socnet.Countries (
	id INT IDENTITY(1,1) NOT NULL,
	name NVARCHAR(60) NOT NULL
)
GO
ALTER TABLE socnet.Countries ADD CONSTRAINT PK_Countries PRIMARY KEY (id);

GO

CREATE TABLE socnet.Cities (
	id INT IDENTITY(1,1) NOT NULL,
	countryId INT NOT NULL,
	name NVARCHAR(120) NOT NULL,
)
GO
ALTER TABLE socnet.Cities ADD CONSTRAINT PK_Cities PRIMARY KEY (id);
GO
ALTER TABLE socnet.Cities ADD CONSTRAINT FK_Cities_Countries_countryId FOREIGN KEY (countryId) REFERENCES socnet.Countries(id);

GO

CREATE TABLE socnet.Occupations (
	id INT IDENTITY(1,1) NOT NULL,
	name NVARCHAR(100) NOT NULL,
)
GO
ALTER TABLE socnet.Occupations ADD CONSTRAINT PK_Occupations PRIMARY KEY (id);

GO

CREATE TABLE socnet.UserDetails (
	id INT NOT NULL,
	profilePicture NVARCHAR(256),
	cityId INT,
	occupationId INT
)
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT PK_UserDetails PRIMARY KEY (id);
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT FK_UserDetails_Users_id FOREIGN KEY (id) REFERENCES socnet.Users(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT FK_UserDetails_Cities_cityId FOREIGN KEY (cityId) REFERENCES socnet.Cities(id);
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT FK_UserDetails_Occupations_occupationId FOREIGN KEY (occupationId) REFERENCES socnet.Occupations(id);

GO

CREATE TABLE socnet.Elements (
	id INT IDENTITY(1, 1) NOT NULL,
	body NTEXT NOT NULL,
	authorId INT NOT NULL,
	creationTime DATETIME NOT NULL,
	elementType NVARCHAR(10) NOT NULL
)
GO
ALTER TABLE socnet.Elements ADD CONSTRAINT PK_Elements PRIMARY KEY CLUSTERED (id);
GO
ALTER TABLE socnet.Elements ADD CONSTRAINT FK_Elements_Users_authorId FOREIGN KEY (authorId) REFERENCES socnet.Users (id);
GO
ALTER TABLE socnet.Elements ADD CONSTRAINT CHK_Elements_elementType CHECK (elementType IN ('post', 'comment'))

GO

CREATE TABLE socnet.Posts (
	id INT NOT NULL,
	visibility NVARCHAR(10) NOT NULL,
	groupId INT NULL
)
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT PK_Posts PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT FK_Posts_Elements_id FOREIGN KEY (id) REFERENCES socnet.Elements (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT FK_Posts_Groups_groupId FOREIGN KEY (groupId) REFERENCES socnet.Groups (id);
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT CHK_Posts_visibility CHECK (visibility IN ('public', 'frndoffrnd', 'friends', 'group'))
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT CHK_Posts_group CHECK ((groupId IS NULL AND visibility = 'group') OR groupId IS NOT NULL)

GO

CREATE TABLE socnet.Edits (
	id INT NOT NULL,
	elementId INT NOT NULL,
	newbody NTEXT NOT NULL,
	editTime DATETIME NOT NULL,
	editorId INT NOT NULL,
)
GO
ALTER TABLE socnet.Edits ADD CONSTRAINT PK_Edits PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Edits ADD CONSTRAINT FK_Edits_Elements_elementId FOREIGN KEY (elementId) REFERENCES socnet.Elements (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Edits ADD CONSTRAINT FK_Edits_Users_editorId FOREIGN KEY (editorId) REFERENCES socnet.Users (id);

GO

CREATE TABLE socnet.Comments (
	id INT NOT NULL,
	postId INT NOT NULL,
	isRemoved BIT NOT NULL DEFAULT 0,
)
GO
ALTER TABLE socnet.Comments ADD CONSTRAINT PK_Comments PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Comments ADD CONSTRAINT FK_Comments_Elements_id FOREIGN KEY (id) REFERENCES socnet.Elements (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Comments ADD CONSTRAINT FK_Comments_Posts_postId FOREIGN KEY (id) REFERENCES socnet.Posts (id);

GO

CREATE TABLE socnet.CommentsComments (
	parentId INT NOT NULL,
	childId INT NOT NULL,
	level INT NOT NULL
)
GO
ALTER TABLE socnet.CommentsComments ADD CONSTRAINT PK_CommentsComments PRIMARY KEY NONCLUSTERED (parentId, childId);
GO
ALTER TABLE socnet.CommentsComments ADD CONSTRAINT FK_CommentsComments_Comments_parentId FOREIGN KEY (parentId) REFERENCES socnet.Comments (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.CommentsComments ADD CONSTRAINT FK_CommentsComments_Comments_childId FOREIGN KEY (childId) REFERENCES socnet.Comments (id);

GO

CREATE TABLE socnet.Reactions (
	userId INT NOT NULL,
	elementId INT NOT NULL,
	creationTime DATETIME NOT NULL,
	type NVARCHAR(10) NOT NULL,
)
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT PK_Reactions PRIMARY KEY NONCLUSTERED (userId, elementId);
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT FK_Reactions_Elements_elementId FOREIGN KEY (elementId) REFERENCES socnet.Elements(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT FK_Reactions_Users_userId FOREIGN KEY (userId) REFERENCES socnet.Users(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT CHK_Reactions_type CHECK (type IN ('like', 'wow', 'haha', 'sad'))

GO

CREATE TABLE socnet.Groups (
	id INT IDENTITY(1,1) NOT NULL,
	name NVARCHAR(100) NOT NULL,
	description NTEXT NOT NULL,
	creationTime DATETIME NOT NULL,
	visibility NVARCHAR(10) NOT NULL
)
GO
ALTER TABLE socnet.Groups ADD CONSTRAINT PK_Groups PRIMARY KEY CLUSTERED (id);
GO
ALTER TABLE socnet.Groups ADD CONSTRAINT CHK_Groups_visibility CHECK (visibility IN ('public', 'private', 'secret'))

GO

CREATE TABLE socnet.UsersGroups (
	userId INT NOT NULL,
	groupId INT NOT NULL,
	joinTime DATETIME NOT NULL
)
GO
ALTER TABLE socnet.UsersGroups ADD CONSTRAINT PK_UsersGroups PRIMARY KEY NONCLUSTERED (userId, groupId);
GO
ALTER TABLE socnet.UsersGroups ADD CONSTRAINT FK_UsersGroups_Users_userId FOREIGN KEY (userId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.UsersGroups ADD CONSTRAINT FK_UsersGroups_Groups_groupId FOREIGN KEY (groupId) REFERENCES socnet.Groups(id) ON DELETE CASCADE;

GO

CREATE TABLE socnet.FriendRequests (
	id INT IDENTITY(1,1) NOT NULL,
	senderId INT NOT NULL,
	receiverId INT NOT NULL,
	sendTime DATETIME NOT NULL,
	status NVARCHAR(10) NOT NULL,
)
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT PK_FriendRequests PRIMARY KEY (id);
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT FK_FriendRequests_Users_senderId FOREIGN KEY (senderId) REFERENCES socnet.Users(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT FK_FriendRequests_Users_receiverId FOREIGN KEY (receiverId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT CHK_FriendRequests_status CHECK (status IN ('pending', 'accepted', 'rejected', 'deleted'));
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT CHK_FriendRequests_self CHECK (senderId <> receiverId);

GO

CREATE TABLE socnet.Friendships (
	firstUserId INT NOT NULL,
	secondUserId INT NOT NULL,
	friendshipStart DATETIME NOT NULL,
	level INT NOT NULL
)
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT PK_Friendships PRIMARY KEY NONCLUSTERED (firstUserId, secondUserId);
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT FK_Friendships_Users_firstUserId FOREIGN KEY (firstUserId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT FK_Friendships_Users_secondUserId FOREIGN KEY (secondUserId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT CHK_Friendships_self CHECK (firstUserId <> secondUserId);

GO

CREATE TABLE socnet.Notifications (
	id INT IDENTITY(1,1) NOT NULL,
	receiverId INT NOT NULL,
	body NTEXT NOT NULL,
	creationTime DATETIME NOT NULL,
	isRead BIT NOT NULL DEFAULT 0
)
GO
ALTER TABLE socnet.Notifications ADD CONSTRAINT PK_Notifications PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Notifications ADD CONSTRAINT FK_Notifications_Users_receiverId FOREIGN KEY (receiverId) REFERENCES socnet.Users(id) ON DELETE CASCADE;
