CREATE DATABASE SocialNetwork2;
GO
USE SocialNetwork2;
GO
CREATE SCHEMA socnet;
GO
-------------------- TABLES --------------------
CREATE TABLE socnet.Users (
	id INT IDENTITY(1,1) NOT NULL,
	firstName NVARCHAR(100) NOT NULL,
	lastName NVARCHAR(100) NOT NULL,
	email NVARCHAR(200) NOT NULL,
	registrationTime DATETIME NOT NULL,
	password NVARCHAR(256) NOT NULL,
)
GO
ALTER TABLE socnet.Users ADD CONSTRAINT PK_Users PRIMARY KEY CLUSTERED (id);

GO

CREATE TABLE socnet.Countries (
	id INT IDENTITY(1,1) NOT NULL,
	name NVARCHAR(60) NOT NULL
)
GO
ALTER TABLE socnet.Countries ADD CONSTRAINT PK_Countries PRIMARY KEY (id);

GO

CREATE TABLE socnet.Cities (
	id INT IDENTITY(1,1) NOT NULL,
	countryId INT NOT NULL,
	name NVARCHAR(120) NOT NULL,
)
GO
ALTER TABLE socnet.Cities ADD CONSTRAINT PK_Cities PRIMARY KEY (id);
GO
ALTER TABLE socnet.Cities ADD CONSTRAINT FK_Cities_Countries_countryId FOREIGN KEY (countryId) REFERENCES socnet.Countries(id);

GO

CREATE TABLE socnet.Occupations (
	id INT IDENTITY(1,1) NOT NULL,
	name NVARCHAR(100) NOT NULL,
)
GO
ALTER TABLE socnet.Occupations ADD CONSTRAINT PK_Occupations PRIMARY KEY (id);

GO

CREATE TABLE socnet.UserDetails (
	id INT NOT NULL,
	profilePicture NVARCHAR(256),
	cityId INT,
	occupationId INT
)
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT PK_UserDetails PRIMARY KEY (id);
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT FK_UserDetails_Users_id FOREIGN KEY (id) REFERENCES socnet.Users(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT FK_UserDetails_Cities_cityId FOREIGN KEY (cityId) REFERENCES socnet.Cities(id);
GO
ALTER TABLE socnet.UserDetails ADD CONSTRAINT FK_UserDetails_Occupations_occupationId FOREIGN KEY (occupationId) REFERENCES socnet.Occupations(id);

GO

CREATE TABLE socnet.Elements (
	id INT IDENTITY(1, 1) NOT NULL,
	body NTEXT NOT NULL,
	authorId INT NOT NULL,
	creationTime DATETIME NOT NULL,
	elementType NVARCHAR(10) NOT NULL
)
GO
ALTER TABLE socnet.Elements ADD CONSTRAINT PK_Elements PRIMARY KEY CLUSTERED (id);
GO
ALTER TABLE socnet.Elements ADD CONSTRAINT FK_Elements_Users_authorId FOREIGN KEY (authorId) REFERENCES socnet.Users (id);
GO
ALTER TABLE socnet.Elements ADD CONSTRAINT CHK_Elements_elementType CHECK (elementType IN ('post', 'comment'))

GO

CREATE TABLE socnet.Posts (
	id INT NOT NULL,
	visibility NVARCHAR(10) NOT NULL,
	groupId INT NULL
)
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT PK_Posts PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT FK_Posts_Elements_id FOREIGN KEY (id) REFERENCES socnet.Elements (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT FK_Posts_Groups_groupId FOREIGN KEY (groupId) REFERENCES socnet.Groups (id);
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT CHK_Posts_visibility CHECK (visibility IN ('public', 'frndoffrnd', 'friends', 'group'))
GO
ALTER TABLE socnet.Posts ADD CONSTRAINT CHK_Posts_group CHECK ((groupId IS NULL AND visibility = 'group') OR groupId IS NOT NULL)

GO

CREATE TABLE socnet.Edits (
	id INT NOT NULL,
	elementId INT NOT NULL,
	newbody NTEXT NOT NULL,
	editTime DATETIME NOT NULL,
	editorId INT NOT NULL,
)
GO
ALTER TABLE socnet.Edits ADD CONSTRAINT PK_Edits PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Edits ADD CONSTRAINT FK_Edits_Elements_elementId FOREIGN KEY (elementId) REFERENCES socnet.Elements (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Edits ADD CONSTRAINT FK_Edits_Users_editorId FOREIGN KEY (editorId) REFERENCES socnet.Users (id);

GO

CREATE TABLE socnet.Comments (
	id INT NOT NULL,
	postId INT NOT NULL,
	isRemoved BIT NOT NULL DEFAULT 0,
)
GO
ALTER TABLE socnet.Comments ADD CONSTRAINT PK_Comments PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Comments ADD CONSTRAINT FK_Comments_Elements_id FOREIGN KEY (id) REFERENCES socnet.Elements (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Comments ADD CONSTRAINT FK_Comments_Posts_postId FOREIGN KEY (id) REFERENCES socnet.Posts (id);

GO

CREATE TABLE socnet.CommentsComments (
	parentId INT NOT NULL,
	childId INT NOT NULL,
	level INT NOT NULL
)
GO
ALTER TABLE socnet.CommentsComments ADD CONSTRAINT PK_CommentsComments PRIMARY KEY NONCLUSTERED (parentId, childId);
GO
ALTER TABLE socnet.CommentsComments ADD CONSTRAINT FK_CommentsComments_Comments_parentId FOREIGN KEY (parentId) REFERENCES socnet.Comments (id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.CommentsComments ADD CONSTRAINT FK_CommentsComments_Comments_childId FOREIGN KEY (childId) REFERENCES socnet.Comments (id);

GO

CREATE TABLE socnet.Reactions (
	userId INT NOT NULL,
	elementId INT NOT NULL,
	creationTime DATETIME NOT NULL,
	type NVARCHAR(10) NOT NULL,
)
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT PK_Reactions PRIMARY KEY NONCLUSTERED (userId, elementId);
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT FK_Reactions_Elements_elementId FOREIGN KEY (elementId) REFERENCES socnet.Elements(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT FK_Reactions_Users_userId FOREIGN KEY (userId) REFERENCES socnet.Users(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.Reactions ADD CONSTRAINT CHK_Reactions_type CHECK (type IN ('like', 'wow', 'haha', 'sad'))

GO

CREATE TABLE socnet.Groups (
	id INT IDENTITY(1,1) NOT NULL,
	name NVARCHAR(100) NOT NULL,
	description NTEXT NOT NULL,
	creationTime DATETIME NOT NULL,
	visibility NVARCHAR(10) NOT NULL
)
GO
ALTER TABLE socnet.Groups ADD CONSTRAINT PK_Groups PRIMARY KEY CLUSTERED (id);
GO
ALTER TABLE socnet.Groups ADD CONSTRAINT CHK_Groups_visibility CHECK (visibility IN ('public', 'private', 'secret'))

GO

CREATE TABLE socnet.UsersGroups (
	userId INT NOT NULL,
	groupId INT NOT NULL,
	joinTime DATETIME NOT NULL
)
GO
ALTER TABLE socnet.UsersGroups ADD CONSTRAINT PK_UsersGroups PRIMARY KEY NONCLUSTERED (userId, groupId);
GO
ALTER TABLE socnet.UsersGroups ADD CONSTRAINT FK_UsersGroups_Users_userId FOREIGN KEY (userId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.UsersGroups ADD CONSTRAINT FK_UsersGroups_Groups_groupId FOREIGN KEY (groupId) REFERENCES socnet.Groups(id) ON DELETE CASCADE;

GO

CREATE TABLE socnet.FriendRequests (
	id INT IDENTITY(1,1) NOT NULL,
	senderId INT NOT NULL,
	receiverId INT NOT NULL,
	sendTime DATETIME NOT NULL,
	status NVARCHAR(10) NOT NULL,
)
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT PK_FriendRequests PRIMARY KEY (id);
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT FK_FriendRequests_Users_senderId FOREIGN KEY (senderId) REFERENCES socnet.Users(id) ON DELETE CASCADE;
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT FK_FriendRequests_Users_receiverId FOREIGN KEY (receiverId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT CHK_FriendRequests_status CHECK (status IN ('pending', 'accepted', 'rejected', 'deleted'));
GO
ALTER TABLE socnet.FriendRequests ADD CONSTRAINT CHK_FriendRequests_self CHECK (senderId <> receiverId);

GO

CREATE TABLE socnet.Friendships (
	firstUserId INT NOT NULL,
	secondUserId INT NOT NULL,
	friendshipStart DATETIME NOT NULL,
	level INT NOT NULL
)
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT PK_Friendships PRIMARY KEY NONCLUSTERED (firstUserId, secondUserId);
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT FK_Friendships_Users_firstUserId FOREIGN KEY (firstUserId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT FK_Friendships_Users_secondUserId FOREIGN KEY (secondUserId) REFERENCES socnet.Users(id);
GO
ALTER TABLE socnet.Friendships ADD CONSTRAINT CHK_Friendships_self CHECK (firstUserId <> secondUserId);

GO

CREATE TABLE socnet.Notifications (
	id INT IDENTITY(1,1) NOT NULL,
	receiverId INT NOT NULL,
	body NTEXT NOT NULL,
	creationTime DATETIME NOT NULL,
	isRead BIT NOT NULL DEFAULT 0
)
GO
ALTER TABLE socnet.Notifications ADD CONSTRAINT PK_Notifications PRIMARY KEY NONCLUSTERED (id);
GO
ALTER TABLE socnet.Notifications ADD CONSTRAINT FK_Notifications_Users_receiverId FOREIGN KEY (receiverId) REFERENCES socnet.Users(id) ON DELETE CASCADE;

GO

-------------------- VIEWS --------------------
CREATE VIEW socnet.V_Top10CommentCount_Posts
AS
SELECT TOP 10 T.postId, T.commentCount, firstName AS authorFirstName, 
Users.lastName AS authorLastName, Posts.visibility AS postVisibility, Groups.name AS groupName FROM (
	SELECT Count(Comments.id) AS commentCount, postId FROM socnet.Comments
	GROUP BY Comments.postId
) T
JOIN socnet.Elements ON Elements.id = T.postId
JOIN socnet.Users ON Users.id = Elements.authorId
JOIN socnet.Posts ON Posts.id = T.postId
RIGHT JOIN socnet.Groups ON Groups.id = Posts.groupId
ORDER BY commentCount DESC

GO

CREATE VIEW socnet.V_Top10UserCount_Groups
AS
SELECT TOP 10 Groups.name as groupName FROM (
	SELECT groupId, Count(*) AS userCount FROM socnet.UsersGroups
	GROUP BY groupId
) T
JOIN socnet.Groups ON Groups.id = T.groupId
ORDER BY userCount DESC

GO

CREATE VIEW socnet.V_Top10ReactionCount_Posts
AS
SELECT TOP 10 T.elementId AS postId, T.reactionCount, Elements.creationTime as postCreationTime, firstName AS authorFirstName, lastName AS authorLastName FROM (
	SELECT Count(*) AS reactionCount, elementId FROM socnet.Reactions
	GROUP BY elementId
) T
JOIN socnet.Elements ON Elements.id = elementId
JOIN socnet.Users ON Users.id = Elements.authorId
WHERE elementType = 'post'
ORDER BY reactionCount DESC

GO

CREATE VIEW socnet.V_Top10ReactionCount_Comments
AS
SELECT TOP 10 T.elementId AS commentId, T.reactionCount, body, Elements.creationTime AS commentCreationTime FROM (
	SELECT Count(*) AS reactionCount, elementId FROM socnet.Reactions
	GROUP BY elementId
) T
JOIN socnet.Elements ON T.elementId = Elements.id
WHERE elementType = 'comment'
ORDER BY reactionCount DESC

GO

CREATE VIEW socnet.V_FriendCounts_Users
AS
SELECT TOP 10 T.userId, firstName, lastName, friendCount FROM (
	SELECT firstUserId AS userId, Count(*) AS friendCount FROM socnet.Friendships
	WHERE level = 0
	GROUP BY firstUserId
) T
JOIN socnet.Users ON Users.id = T.userId

GO

CREATE VIEW socnet.V_Count_FriendRequests
AS
SELECT status, Count(*) AS requestCount FROM socnet.FriendRequests
GROUP BY FriendRequests.status

GO

CREATE VIEW socnet.V_FriendCountsRanges_Users
AS
SELECT T.countRange FROM (
	SELECT CASE
		WHEN friendCount < 10 THEN '<10'
		WHEN friendCount BETWEEN 10 AND 49 THEN '10-49'
		WHEN friendCount BETWEEN 50 AND 99 THEN '50-99'
		WHEN friendCount >= 100 THEN '>=100'
	END AS countRange FROM socnet.V_FriendCounts_Users
) T

GO

CREATE VIEW socnet.V_UsersCreatedRanges_MonthYear
AS
SELECT DATEPART(YEAR, registrationTime) AS Year, DATEPART(MONTH, registrationTime) AS Month, Count(*) AS userCount
FROM socnet.Users
GROUP BY DATEPART(MONTH, registrationTime), DATEPART(YEAR, registrationTime)

GO

CREATE VIEW socnet.V_Top10EditCount_User
AS
SELECT TOP 10 T.editorId, firstName, lastName, T.editCount FROM (
	SELECT editorId, Count(*) AS editCount FROM socnet.Edits
	GROUP BY editorId
) T
JOIN socnet.Users ON Users.id = T.editorId
ORDER BY editCount DESC

GO

-- average time to comments since posting in seconds
CREATE VIEW socnet.V_Top10AvgCommentTime_Posts
AS
SELECT Avg(DATEDIFF(SECOND, Elements.creationTime, PostElement.creationTime)) AS avgCommentTimeSeconds FROM socnet.Comments
JOIN socnet.Elements PostElement ON PostElement.id = Comments.postId
JOIN socnet.Elements ON Elements.id = Comments.id
GROUP BY postId

GO

-------------------- FUNCTIONS --------------------

CREATE FUNCTION socnet.FN_Reactions_Element(@id INT)
RETURNS TABLE
AS
RETURN (
	SELECT Count(*) AS reactionCount, type
	FROM Reactions
	WHERE elementId = @id
	GROUP BY Reactions.type
)

GO

CREATE FUNCTION socnet.FN_Comments_Post(@id INT)
RETURNS TABLE
AS
RETURN (
	SELECT id FROM Comments 
	WHERE postId = @id AND Comments.id NOT IN (SELECT CommentsComments.childId FROM CommentsComments)
)

GO

CREATE FUNCTION socnet.FN_UnreadNotifications_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT body, creationTime FROM socnet.Notifications WHERE receiverId = @userid AND isRead = 0
)

GO

CREATE FUNCTION socnet.FN_EditHistory(@elementid INT)
RETURNS TABLE
AS
RETURN (
	SELECT newbody, editTime, Users.firstName AS editorFirstName, Users.lastName AS editorLastName
	FROM socnet.Edits
	JOIN socnet.Users ON editorId = Users.id
	WHERE elementId = @elementid
)

GO

CREATE FUNCTION socnet.FN_PostsAuthored_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Elements.body, Elements.creationTime, Posts.visibility, (CASE WHEN Posts.groupId IS NOT NULL THEN Groups.name ELSE '' END) AS 'Group' FROM Elements
	JOIN Posts ON Elements.id = Posts.id
	LEFT JOIN Groups ON Groups.id = Posts.groupId
	WHERE elementType = 'post' AND authorId = @userid
)

GO

CREATE FUNCTION socnet.FN_CommentsAuthored_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Elements.body, Elements.creationTime, PostElement.body AS postBody, PostAuthor.firstName AS postAuthorFirstName, PostAuthor.lastName AS postAuthorLastName FROM socnet.Elements
	JOIN Comments ON Elements.id = Comments.id
	JOIN Elements PostElement ON Elements.id = Comments.postId
	JOIN Users AS PostAuthor ON PostAuthor.id = PostElement.authorId
	WHERE Elements.authorId = @userid
)

GO

CREATE FUNCTION socnet.FN_PendingSent_FriendRequests_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT receiverId, sendTime FROM FriendRequests 
	WHERE senderId = @userid AND status = 'pending'
)

GO

CREATE FUNCTION socnet.FN_PendingReceived_FriendRequests_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT senderId, sendTime FROM FriendRequests
	WHERE receiverId = @userid AND status = 'pending'
)

GO

CREATE FUNCTION socnet.FN_Friends_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Users.id, Users.firstName, Users.lastName FROM Friendships
	JOIN Users ON Users.id = Friendships.secondUserId
	WHERE firstUserId = @userId AND level = 0
)

GO

CREATE FUNCTION socnet.FN_FriendsOfFriends_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Users.id, Users.firstName, Users.lastName FROM Friendships
	JOIN Users ON Users.id = Friendships.secondUserId
	WHERE firstUserId = @userId AND level = 1
)

GO

-------------------- PROCEDURES --------------------

CREATE PROC socnet.SP_LatestBody_Element(@elementid INT, @body NTEXT OUT, @isEdited BIT OUT)
AS
BEGIN
	SET @body = (SELECT TOP 1 newbody FROM socnet.Edits
		WHERE elementId = @elementid
		ORDER BY editTime DESC);
	SET @isEdited = 1
	IF @body IS NULL
		SET @body = (SELECT body FROM socnet.Elements WHERE Elements.id = @elementid)
		SET @isEdited = 0
END

GO

CREATE PROC socnet.SP_Delete_Post(@postid INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		-- delete comments first (CommentsComments and Edits will cascade)
		DELETE FROM socnet.Elements WHERE elementType = 'comment' AND id IN (SELECT id FROM Comments WHERE postId = @postid);

		-- delete post itself (Edits will cascade)
		DELETE FROM socnet.Elements WHERE elementType = 'post' AND id = @postid;
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Delete_Comment(@commentid INT)
AS
UPDATE socnet.Comments SET isRemoved = 1 WHERE id = @commentid

GO

CREATE PROC socnet.SP_Delete_User(@userid INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		-- 'removed user' takes over user's comments
		UPDATE socnet.Elements SET authorId = 1 WHERE id IN (SELECT id FROM Elements WHERE authorId = @userid);

		-- remove nongroup posts (will cascade to comments)
		DELETE FROM Elements WHERE elementType = 'post' AND authorId = @userid AND id IN (SELECT id FROM Posts WHERE groupId IS NULL)

		-- Notifications, FriendRequests will cascade
		DELETE FROM Users WHERE id = @userid;
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Delete_Group(@groupid INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		-- delete comments from every post in group (reactions and edits will cascade)
		DELETE FROM Elements WHERE elementType = 'comment' AND Elements.id IN (SELECT Comments.id FROM Comments JOIN Posts ON postId = Posts.id WHERE groupId = @groupid)

		-- delete group posts (Edits, Posts will cascade)
		DELETE FROM Elements WHERE elementType = 'post' AND Elements.id IN (SELECT Posts.id FROM Posts WHERE groupId = @groupid)

		-- send notification to all users
		DECLARE @groupName NVARCHAR(100) = (SELECT TOP 1 name FROM Groups WHERE id = @groupid);
		INSERT INTO socnet.Notifications (receiverId, body, creationTime)
		SELECT userId, FORMATMESSAGE('Group "%s" was removed.', @groupName), GETDATE() FROM socnet.UsersGroups WHERE groupId = @groupid;
		
		-- delete group (UsersGroups will cascade)
		DELETE FROM Groups WHERE id = @groupid
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Add_Post(@body NTEXT, @authorId INT, @visibility NVARCHAR(10), @groupId INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		INSERT INTO Elements (body, authorId, creationTime, elementType) VALUES (@body, @authorId, GETDATE(), 'post');
		DECLARE @newPostId INT = SCOPE_IDENTITY();
		INSERT INTO Posts (id, groupId, visibility) VALUES (@newPostId, @groupId, @visibility);
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Add_Comment(@body NTEXT, @authorId INT, @postId INT, @parentCommentId INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		INSERT INTO Elements (body, authorId, creationTime, elementType) VALUES (@body, @authorId, GETDATE(), 'comment');
		DECLARE @newCommentId INT = SCOPE_IDENTITY();
		INSERT INTO Comments (id, postId) VALUES (@newCommentId, @postId);

		-- updating Comments' parents
		IF @parentCommentId IS NOT NULL
			INSERT INTO CommentsComments (parentId, childId, level) VALUES (@parentCommentId, @newCommentId, 0);
			INSERT INTO CommentsComments (parentId, childId, level)
			SELECT parentId, childId, level + 1 FROM CommentsComments WHERE childId = @parentCommentId;
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Add_User(@firstName NVARCHAR(100), @lastName NVARCHAR(100), @email NVARCHAR(200), @password NVARCHAR(256))
AS
BEGIN
	INSERT INTO Users (firstName, lastName, email, password, registrationTime) VALUES (@firstName, @lastName, @email, @password, GETDATE());
END

GO

CREATE PROC socnet.SP_Send_FriendRequest(@senderId INT, @receiverId INT)
AS
BEGIN
	INSERT INTO FriendRequests (senderId, receiverId, sendTime, status) VALUES (@senderId, @receiverId, GETDATE(), 'pending')
END

GO

CREATE PROC socnet.SP_SendResponse_FriendRequest(@requestId INT, @response NVARCHAR(10))
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		IF @response <> 'pending'
			UPDATE FriendRequests SET status = @response WHERE id = @requestId;

		-- add friendship and update friends-of-friends
		IF @response = 'accepted'
			DECLARE @senderId INT, @receiverId INT;
			SELECT @senderId = FriendRequests.senderId, @receiverId = FriendRequests.receiverId FROM FriendRequests WHERE FriendRequests.id = @requestId;
		
			-- Trigger will insert other way
			IF EXISTS (SELECT * FROM Friendships WHERE firstUserId = @senderId AND secondUserId = @receiverId)
				UPDATE Friendships SET level = 0, friendshipStart = GETDATE() WHERE firstUserId = @senderId AND secondUserId = @receiverId
			ELSE
				INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level) VALUES (@senderId, @receiverId, GETDATE(), 0);

			-- add 0-level friends of @receiver as 1-level friends of @sender
			INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level)
			SELECT @senderId, secondUserId, GETDATE(), 1 FROM Friendships 
			WHERE firstUserId = @receiverId AND level = 0 AND secondUserId NOT IN (SELECT secondUserId FROM Friendships WHERE firstUserId = @senderId AND level = 0)

			-- add 0-level friends of @sender as 1-level friends of @receiver
			INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level)
			SELECT firstUserId, @receiverId, GETDATE(), 1 FROM Friendships 
			WHERE secondUserId = @senderId AND level = 0 AND firstUserId NOT IN (SELECT firstUserId FROM Friendships WHERE secondUserId = @receiverId AND level = 0)
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Edit_Element(@elementId INT, @newbody NTEXT, @editorId INT)
AS
BEGIN
	INSERT INTO socnet.Edits (elementId, newbody, editorId, editTime) VALUES (@elementId, @newbody, @editorId, GETDATE());
END

GO

CREATE PROC socnet.SP_Add_Reaction(@elementId INT, @userId INT, @type NVARCHAR(10))
AS
BEGIN
	INSERT INTO Reactions (elementId, userId, type, creationTime) VALUES (@elementId, @userId, @type, GETDATE());
END

GO

CREATE PROC socnet.SP_Delete_Reaction(@userId INT, @elementId INT)
AS
BEGIN
	DELETE FROM Reactions WHERE userId = @userId AND @elementId = @elementId
END

GO

CREATE PROC socnet.SP_Add_Group(@name NVARCHAR(100), @description NTEXT, @visibility NVARCHAR(10))
AS
BEGIN
	INSERT INTO socnet.Groups (name, description, visibility, creationTime) VALUES (@name, @description, @visibility, GETDATE());
END

GO

CREATE PROC socnet.SP_Update_UserDetails(@userid INT, @profilePicture NVARCHAR(256), @occupation NVARCHAR(60), @city NVARCHAR(60), @country NVARCHAR(60))
AS
BEGIN
	-- add occupation if not present, save id
	DECLARE @occupationId INT = NULL;
	IF @occupation NOT IN (SELECT name FROM Occupations)
	BEGIN
		INSERT INTO Occupations (name) VALUES (@occupation);
		SET @occupationId = SCOPE_IDENTITY();
	END
	ELSE
		SET @occupationId = (SELECT TOP 1 id FROM Occupations WHERE name = @occupation);

	-- add country if not present, save id
	DECLARE @countryId INT = NULL;
	IF @country NOT IN (SELECT name FROM Countries)
	BEGIN
		INSERT INTO Cities (name) VALUES (@occupation);
		SET @countryId = SCOPE_IDENTITY();
	END
	ELSE
		SET @countryId = (SELECT TOP 1 id FROM Countries WHERE name = @country);

	-- add city if not present, save id
	DECLARE @cityId INT = NULL;
	IF @city NOT IN (SELECT name FROM Cities WHERE countryId = @countryId)
	BEGIN
		INSERT INTO Cities (name, countryId) VALUES (@occupation, @countryId);
		SET @cityId = SCOPE_IDENTITY();
	END
	ELSE
		SET @cityId = (SELECT TOP 1 id FROM Cities WHERE name = @city AND countryId = @countryId);

	-- update details
	UPDATE UserDetails SET profilePicture = @profilePicture, cityId = @cityId, occupationId = @occupationId WHERE id = @userid
	IF @@ROWCOUNT = 0
		INSERT INTO UserDetails (id, profilePicture, cityId, occupationId) VALUES (@userid, @profilePicture, @cityId, @occupationId)
END

GO

-------------------- TRIGGERS --------------------

CREATE TRIGGER socnet.TR_AI_FriendRequests
ON socnet.FriendRequests
AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT id FROM inserted
		WHERE status NOT IN ('rejected', 'deleted') AND senderId IN (SELECT FriendRequests.senderId FROM FriendRequests)
		AND receiverId IN (SELECT FriendRequests.receiverId FROM FriendRequests)
	)
		ROLLBACK
END

GO

CREATE TRIGGER socnet.TR_AI_Friendships
ON socnet.Friendships
AFTER INSERT
AS
BEGIN
	-- if duplicate then rollback
	IF EXISTS (
		SELECT firstUserId, secondUserId FROM inserted
		WHERE firstUserId IN (SELECT firstUserId FROM Friendships)
		AND secondUserId IN (SELECT firstUserId FROM Friendships)
	)
		ROLLBACK

	-- add reversed pair
	INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level)
	SELECT secondUserId, firstUserId, friendshipStart, level FROM inserted;
END

GO

CREATE TRIGGER socnet.TR_AD_Friendships
ON socnet.Friendships
AFTER DELETE
AS
BEGIN
	UPDATE FriendRequests SET status = 'deleted'
	WHERE status = 'accepted'
	AND (senderId IN (SELECT firstUserId FROM deleted) OR receiverId IN (SELECT firstUserId FROM deleted))
	-- delete reversed pair
	DELETE FROM Friendships WHERE secondUserId IN (SELECT firstUserId FROM deleted)
END

GO

CREATE TRIGGER socnet.TR_AI_Posts
ON socnet.Posts
AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT inserted.id FROM inserted JOIN Elements ON inserted.id = Elements.id WHERE elementType <> 'post'
	)
		ROLLBACK
END

GO

CREATE TRIGGER socnet.TR_AI_Comments
ON socnet.Comments
AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT inserted.id FROM inserted JOIN Elements ON inserted.id = Elements.id WHERE elementType <> 'comment'
	)
		ROLLBACK
END