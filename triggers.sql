-------------------- TRIGGERS --------------------

CREATE TRIGGER socnet.TR_AI_FriendRequests
ON socnet.FriendRequests
AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT id FROM inserted
		WHERE status NOT IN ('rejected', 'deleted') AND senderId IN (SELECT FriendRequests.senderId FROM FriendRequests)
		AND receiverId IN (SELECT FriendRequests.receiverId FROM FriendRequests)
	)
		ROLLBACK
END

GO

CREATE TRIGGER socnet.TR_AI_Friendships
ON socnet.Friendships
AFTER INSERT
AS
BEGIN
	-- if duplicate then rollback
	IF EXISTS (
		SELECT firstUserId, secondUserId FROM inserted
		WHERE firstUserId IN (SELECT firstUserId FROM Friendships)
		AND secondUserId IN (SELECT firstUserId FROM Friendships)
	)
		ROLLBACK

	-- add reversed pair
	INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level)
	SELECT secondUserId, firstUserId, friendshipStart, level FROM inserted;
END

GO

CREATE TRIGGER socnet.TR_AD_Friendships
ON socnet.Friendships
AFTER DELETE
AS
BEGIN
	UPDATE FriendRequests SET status = 'deleted'
	WHERE status = 'accepted'
	AND (senderId IN (SELECT firstUserId FROM deleted) OR receiverId IN (SELECT firstUserId FROM deleted))
	-- delete reversed pair
	DELETE FROM Friendships WHERE secondUserId IN (SELECT firstUserId FROM deleted)
END

GO

CREATE TRIGGER socnet.TR_AI_Posts
ON socnet.Posts
AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT inserted.id FROM inserted JOIN Elements ON inserted.id = Elements.id WHERE elementType <> 'post'
	)
		ROLLBACK
END

GO

CREATE TRIGGER socnet.TR_AI_Comments
ON socnet.Comments
AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT inserted.id FROM inserted JOIN Elements ON inserted.id = Elements.id WHERE elementType <> 'comment'
	)
		ROLLBACK
END