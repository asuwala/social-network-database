-------------------- PROCEDURES --------------------

CREATE PROC socnet.SP_LatestBody_Element(@elementid INT, @body NTEXT OUT, @isEdited BIT OUT)
AS
BEGIN
	SET @body = (SELECT TOP 1 newbody FROM socnet.Edits
		WHERE elementId = @elementid
		ORDER BY editTime DESC);
	SET @isEdited = 1
	IF @body IS NULL
		SET @body = (SELECT body FROM socnet.Elements WHERE Elements.id = @elementid)
		SET @isEdited = 0
END

GO

CREATE PROC socnet.SP_Delete_Post(@postid INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		-- delete comments first (CommentsComments and Edits will cascade)
		DELETE FROM socnet.Elements WHERE elementType = 'comment' AND id IN (SELECT id FROM Comments WHERE postId = @postid);

		-- delete post itself (Edits will cascade)
		DELETE FROM socnet.Elements WHERE elementType = 'post' AND id = @postid;
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Delete_Comment(@commentid INT)
AS
UPDATE socnet.Comments SET isRemoved = 1 WHERE id = @commentid

GO

CREATE PROC socnet.SP_Delete_User(@userid INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		-- 'removed user' takes over user's comments
		UPDATE socnet.Elements SET authorId = 1 WHERE id IN (SELECT id FROM Elements WHERE authorId = @userid);

		-- remove nongroup posts (will cascade to comments)
		DELETE FROM Elements WHERE elementType = 'post' AND authorId = @userid AND id IN (SELECT id FROM Posts WHERE groupId IS NULL)

		-- Notifications, FriendRequests will cascade
		DELETE FROM Users WHERE id = @userid;
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Delete_Group(@groupid INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		-- delete comments from every post in group (reactions and edits will cascade)
		DELETE FROM Elements WHERE elementType = 'comment' AND Elements.id IN (SELECT Comments.id FROM Comments JOIN Posts ON postId = Posts.id WHERE groupId = @groupid)

		-- delete group posts (Edits, Posts will cascade)
		DELETE FROM Elements WHERE elementType = 'post' AND Elements.id IN (SELECT Posts.id FROM Posts WHERE groupId = @groupid)

		-- send notification to all users
		DECLARE @groupName NVARCHAR(100) = (SELECT TOP 1 name FROM Groups WHERE id = @groupid);
		INSERT INTO socnet.Notifications (receiverId, body, creationTime)
		SELECT userId, FORMATMESSAGE('Group "%s" was removed.', @groupName), GETDATE() FROM socnet.UsersGroups WHERE groupId = @groupid;
		
		-- delete group (UsersGroups will cascade)
		DELETE FROM Groups WHERE id = @groupid
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Add_Post(@body NTEXT, @authorId INT, @visibility NVARCHAR(10), @groupId INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		INSERT INTO Elements (body, authorId, creationTime, elementType) VALUES (@body, @authorId, GETDATE(), 'post');
		DECLARE @newPostId INT = SCOPE_IDENTITY();
		INSERT INTO Posts (id, groupId, visibility) VALUES (@newPostId, @groupId, @visibility);
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Add_Comment(@body NTEXT, @authorId INT, @postId INT, @parentCommentId INT)
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		INSERT INTO Elements (body, authorId, creationTime, elementType) VALUES (@body, @authorId, GETDATE(), 'comment');
		DECLARE @newCommentId INT = SCOPE_IDENTITY();
		INSERT INTO Comments (id, postId) VALUES (@newCommentId, @postId);

		-- updating Comments' parents
		IF @parentCommentId IS NOT NULL
			INSERT INTO CommentsComments (parentId, childId, level) VALUES (@parentCommentId, @newCommentId, 0);
			INSERT INTO CommentsComments (parentId, childId, level)
			SELECT parentId, childId, level + 1 FROM CommentsComments WHERE childId = @parentCommentId;
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Add_User(@firstName NVARCHAR(100), @lastName NVARCHAR(100), @email NVARCHAR(200), @password NVARCHAR(256))
AS
BEGIN
	INSERT INTO Users (firstName, lastName, email, password, registrationTime) VALUES (@firstName, @lastName, @email, @password, GETDATE());
END

GO

CREATE PROC socnet.SP_Send_FriendRequest(@senderId INT, @receiverId INT)
AS
BEGIN
	INSERT INTO FriendRequests (senderId, receiverId, sendTime, status) VALUES (@senderId, @receiverId, GETDATE(), 'pending')
END

GO

CREATE PROC socnet.SP_SendResponse_FriendRequest(@requestId INT, @response NVARCHAR(10))
AS
BEGIN
	SET XACT_ABORT ON
	BEGIN TRAN
		IF @response <> 'pending'
			UPDATE FriendRequests SET status = @response WHERE id = @requestId;

		-- add friendship and update friends-of-friends
		IF @response = 'accepted'
			DECLARE @senderId INT, @receiverId INT;
			SELECT @senderId = FriendRequests.senderId, @receiverId = FriendRequests.receiverId FROM FriendRequests WHERE FriendRequests.id = @requestId;
		
			-- Trigger will insert other way
			IF EXISTS (SELECT * FROM Friendships WHERE firstUserId = @senderId AND secondUserId = @receiverId)
				UPDATE Friendships SET level = 0, friendshipStart = GETDATE() WHERE firstUserId = @senderId AND secondUserId = @receiverId
			ELSE
				INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level) VALUES (@senderId, @receiverId, GETDATE(), 0);

			-- add 0-level friends of @receiver as 1-level friends of @sender
			INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level)
			SELECT @senderId, secondUserId, GETDATE(), 1 FROM Friendships 
			WHERE firstUserId = @receiverId AND level = 0 AND secondUserId NOT IN (SELECT secondUserId FROM Friendships WHERE firstUserId = @senderId AND level = 0)

			-- add 0-level friends of @sender as 1-level friends of @receiver
			INSERT INTO Friendships (firstUserId, secondUserId, friendshipStart, level)
			SELECT firstUserId, @receiverId, GETDATE(), 1 FROM Friendships 
			WHERE secondUserId = @senderId AND level = 0 AND firstUserId NOT IN (SELECT firstUserId FROM Friendships WHERE secondUserId = @receiverId AND level = 0)
	COMMIT
	SET XACT_ABORT OFF
END

GO

CREATE PROC socnet.SP_Edit_Element(@elementId INT, @newbody NTEXT, @editorId INT)
AS
BEGIN
	INSERT INTO socnet.Edits (elementId, newbody, editorId, editTime) VALUES (@elementId, @newbody, @editorId, GETDATE());
END

GO

CREATE PROC socnet.SP_Add_Reaction(@elementId INT, @userId INT, @type NVARCHAR(10))
AS
BEGIN
	INSERT INTO Reactions (elementId, userId, type, creationTime) VALUES (@elementId, @userId, @type, GETDATE());
END

GO

CREATE PROC socnet.SP_Delete_Reaction(@userId INT, @elementId INT)
AS
BEGIN
	DELETE FROM Reactions WHERE userId = @userId AND @elementId = @elementId
END

GO

CREATE PROC socnet.SP_Add_Group(@name NVARCHAR(100), @description NTEXT, @visibility NVARCHAR(10))
AS
BEGIN
	INSERT INTO socnet.Groups (name, description, visibility, creationTime) VALUES (@name, @description, @visibility, GETDATE());
END

GO

CREATE PROC socnet.SP_Update_UserDetails(@userid INT, @profilePicture NVARCHAR(256), @occupation NVARCHAR(60), @city NVARCHAR(60), @country NVARCHAR(60))
AS
BEGIN
	-- add occupation if not present, save id
	DECLARE @occupationId INT = NULL;
	IF @occupation NOT IN (SELECT name FROM Occupations)
	BEGIN
		INSERT INTO Occupations (name) VALUES (@occupation);
		SET @occupationId = SCOPE_IDENTITY();
	END
	ELSE
		SET @occupationId = (SELECT TOP 1 id FROM Occupations WHERE name = @occupation);

	-- add country if not present, save id
	DECLARE @countryId INT = NULL;
	IF @country NOT IN (SELECT name FROM Countries)
	BEGIN
		INSERT INTO Cities (name) VALUES (@occupation);
		SET @countryId = SCOPE_IDENTITY();
	END
	ELSE
		SET @countryId = (SELECT TOP 1 id FROM Countries WHERE name = @country);

	-- add city if not present, save id
	DECLARE @cityId INT = NULL;
	IF @city NOT IN (SELECT name FROM Cities WHERE countryId = @countryId)
	BEGIN
		INSERT INTO Cities (name, countryId) VALUES (@occupation, @countryId);
		SET @cityId = SCOPE_IDENTITY();
	END
	ELSE
		SET @cityId = (SELECT TOP 1 id FROM Cities WHERE name = @city AND countryId = @countryId);

	-- update details
	UPDATE UserDetails SET profilePicture = @profilePicture, cityId = @cityId, occupationId = @occupationId WHERE id = @userid
	IF @@ROWCOUNT = 0
		INSERT INTO UserDetails (id, profilePicture, cityId, occupationId) VALUES (@userid, @profilePicture, @cityId, @occupationId)
END