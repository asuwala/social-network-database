-------------------- FUNCTIONS --------------------

CREATE FUNCTION socnet.FN_Reactions_Element(@id INT)
RETURNS TABLE
AS
RETURN (
	SELECT Count(*) AS reactionCount, type
	FROM Reactions
	WHERE elementId = @id
	GROUP BY Reactions.type
)

GO

CREATE FUNCTION socnet.FN_Comments_Post(@id INT)
RETURNS TABLE
AS
RETURN (
	SELECT id FROM Comments 
	WHERE postId = @id AND Comments.id NOT IN (SELECT CommentsComments.childId FROM CommentsComments)
)

GO

CREATE FUNCTION socnet.FN_UnreadNotifications_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT body, creationTime FROM socnet.Notifications WHERE receiverId = @userid AND isRead = 0
)

GO

CREATE FUNCTION socnet.FN_EditHistory(@elementid INT)
RETURNS TABLE
AS
RETURN (
	SELECT newbody, editTime, Users.firstName AS editorFirstName, Users.lastName AS editorLastName
	FROM socnet.Edits
	JOIN socnet.Users ON editorId = Users.id
	WHERE elementId = @elementid
)

GO

CREATE FUNCTION socnet.FN_PostsAuthored_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Elements.body, Elements.creationTime, Posts.visibility, (CASE WHEN Posts.groupId IS NOT NULL THEN Groups.name ELSE '' END) AS 'Group' FROM Elements
	JOIN Posts ON Elements.id = Posts.id
	LEFT JOIN Groups ON Groups.id = Posts.groupId
	WHERE elementType = 'post' AND authorId = @userid
)

GO

CREATE FUNCTION socnet.FN_CommentsAuthored_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Elements.body, Elements.creationTime, PostElement.body AS postBody, PostAuthor.firstName AS postAuthorFirstName, PostAuthor.lastName AS postAuthorLastName FROM socnet.Elements
	JOIN Comments ON Elements.id = Comments.id
	JOIN Elements PostElement ON Elements.id = Comments.postId
	JOIN Users AS PostAuthor ON PostAuthor.id = PostElement.authorId
	WHERE Elements.authorId = @userid
)

GO

CREATE FUNCTION socnet.FN_PendingSent_FriendRequests_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT receiverId, sendTime FROM FriendRequests 
	WHERE senderId = @userid AND status = 'pending'
)

GO

CREATE FUNCTION socnet.FN_PendingReceived_FriendRequests_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT senderId, sendTime FROM FriendRequests
	WHERE receiverId = @userid AND status = 'pending'
)

GO

CREATE FUNCTION socnet.FN_Friends_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Users.id, Users.firstName, Users.lastName FROM Friendships
	JOIN Users ON Users.id = Friendships.secondUserId
	WHERE firstUserId = @userId AND level = 0
)

GO

CREATE FUNCTION socnet.FN_FriendsOfFriends_User(@userid INT)
RETURNS TABLE
AS
RETURN (
	SELECT Users.id, Users.firstName, Users.lastName FROM Friendships
	JOIN Users ON Users.id = Friendships.secondUserId
	WHERE firstUserId = @userId AND level = 1
)