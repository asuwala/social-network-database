# social-network-database

### 1. Temat projektu
Projekt bazy danych sieci społecznościowej. Projekt przewiduje funkcjonalności:
* zawieranie znajomości między użytkownikami
* tworzenie grup użytkowników
* dodawanie postów zarówno publicznych jak i tylko dla znajomych oraz grupowych
* dodawanie komentarzy do postów i innych komentarzy
* możliwość reakcji na post/komentarz (like, wow, etc.)
* możliwość dodania miasta i zawodu w profilu

### 2. Przyjęte ograniczenia:
* imię, nazwisko, nazwa grupy, nazwa miasta, nazwa kraju, nazwa zawodu są ograniczone do 100 znaków
* email do 200 znaków
* nie można być znajomym z samym sobą
* nie można wysłać kolejnego zaproszenia do znajomych jeżeli już jest wysłane
* grupa musi mieć przynajmniej jednego użytkownika, nie musi natomiast mieć postów
* każdy użytkownik może dać jedną reakcję na post/komentarz niezależnie od typu

### 3. ER
![erd](erd.JPG)
### 4. Schemat bazy danych
![schema_diagram](schema_diagram.JPG)

### 5. Opis widoków
* V_Top10CommentCount_Posts - 10 najbardziej aktywnych (najwięcej komentarzy) postów wraz z ich id, autorem i ewentualnie grupą do której należą
* V_Top10UserCount_Groups - 10 grup o największej liczbie użytkowników
* V_Top10ReactionCount_Posts - 10 postów o największej liczbie reakcji
* V_Top10ReactionCount_Comments - 10 komentarzy o największej liczbie reakcji
* V_FriendCounts_Users - imię i nazwisko użytkowników wraz z ich liczbą znajomych
* V_Count_FriendRequests - liczba akceptowanych, odrzuconych i oczekujących zaproszeń do znajomych
* V_FriendCountsRanges_Users - zlicza userów o znajomych w przedziale (<10, 10-49, 50-99, >100)
* V_UsersCreatedRanges_MonthYear - zlicza stworzone konta z podziałem na miesiąc i rok założenia
* V_Top10EditCount_User - 10 użytkowników najczęściej edytujących posty/komentarze
* V_Top10AvgCommentTime_Posts - 10 postów z najniższym średnim czasem do napisania komentarza

### 6. Opis funkcji
* FN_Reactions_Element - zwraca reakcje pod postem/komentarzem
* FN_Comments_Post - zwraca komentarze pod postem
* FN_UnreadNotifications_User - zwraca wszystkie nieodczytane powiadomienia użytkownika
* FN_EditHistory - zwraca wszystkie wersje elementu
* FN_PostsAuthored_User - zwraca posty stworzone przez użytkownika
* FN_CommentsAuthored_User - zwraca komentarze napisane przez użytkownika
* FN_PendingSent_FriendRequests_User - zwraca oczekujące zaproszenia wysłane przez użytkownika
* FN_PendingReceived_FriendRequests_User - zwraca oczekujące zaproszenia otrzymane przez użytkownika
* FN_Friends_User - zwraca znajomych użytkownika
* FN_FriendsOfFriends_User - zwraca znajomych znajomych użytkownika

### 7. Opis procedur
* SP_LatestBody_Element - pobiera najnowszą treść elementu
* SP_Delete_Post - usuwa post ze wszystkimi komentarzami i reakcjami
* SP_Delete_Comment - zaznacza komentarz jako usunięty
* SP_Delete_User - usuwa użytkownika wraz z niegrupowymi postami, grupowe posty przejmuje specjalny użytkownik
* SP_Delete_Group - usuwa grupę i wszystkie posty, wysyła powiadomienie do użytkowników
* SP_Add_Post - dodaje element i odpowiadający mu post
* SP_Add_Comment - dodaje element i odpowiadający mu komentarz
* SP_Add_User - dodaje użytkownika
* SP_Send_FriendRequest - dodaje zaproszenie do znajomych
* SP_SendResponse_FriendRequest - aktualizuje status zaproszenia, ewentualnie dodaje znajomego
* SP_Edit_Element - dodaje wpis zmieniający treść elementu
* SP_Add_Reaction - dodaje reakcję
* SP_Delete_Reaction - usuwa reakcję
* SP_Add_Group - dodaje grupę 
* SP_Update_UserDetails - aktualizuje (ewentualnie dodaje wpis) dodatkowe informacje o użytkowniku. Jeżeli nie występują to dodaje także wpisy o mieście, kraju i zawodzie

### 8. Opis wyzwalaczy
* TR_AI_FriendRequests - blokuje duplikaty z wyjątkiem statusu 'rejected' i 'deleted'
* TR_AI_Friendships - dodaje odwróconą parę
* TR_AD_Friendships - kasuje odpowiadającą odwróconą parę
* TR_AI_Posts - weryfikuje czy odpowiadajcy elementType to 'post'
* TR_AI_Comments - weryfikuje czy odpowiadający elementType to 'comment'