-------------------- VIEWS --------------------
CREATE VIEW socnet.V_Top10CommentCount_Posts
AS
SELECT TOP 10 T.postId, T.commentCount, firstName AS authorFirstName, 
Users.lastName AS authorLastName, Posts.visibility AS postVisibility, Groups.name AS groupName FROM (
	SELECT Count(Comments.id) AS commentCount, postId FROM socnet.Comments
	GROUP BY Comments.postId
) T
JOIN socnet.Elements ON Elements.id = T.postId
JOIN socnet.Users ON Users.id = Elements.authorId
JOIN socnet.Posts ON Posts.id = T.postId
RIGHT JOIN socnet.Groups ON Groups.id = Posts.groupId
ORDER BY commentCount DESC

GO

CREATE VIEW socnet.V_Top10UserCount_Groups
AS
SELECT TOP 10 Groups.name as groupName FROM (
	SELECT groupId, Count(*) AS userCount FROM socnet.UsersGroups
	GROUP BY groupId
) T
JOIN socnet.Groups ON Groups.id = T.groupId
ORDER BY userCount DESC

GO

CREATE VIEW socnet.V_Top10ReactionCount_Posts
AS
SELECT TOP 10 T.elementId AS postId, T.reactionCount, Elements.creationTime as postCreationTime, firstName AS authorFirstName, lastName AS authorLastName FROM (
	SELECT Count(*) AS reactionCount, elementId FROM socnet.Reactions
	GROUP BY elementId
) T
JOIN socnet.Elements ON Elements.id = elementId
JOIN socnet.Users ON Users.id = Elements.authorId
WHERE elementType = 'post'
ORDER BY reactionCount DESC

GO

CREATE VIEW socnet.V_Top10ReactionCount_Comments
AS
SELECT TOP 10 T.elementId AS commentId, T.reactionCount, body, Elements.creationTime AS commentCreationTime FROM (
	SELECT Count(*) AS reactionCount, elementId FROM socnet.Reactions
	GROUP BY elementId
) T
JOIN socnet.Elements ON T.elementId = Elements.id
WHERE elementType = 'comment'
ORDER BY reactionCount DESC

GO

CREATE VIEW socnet.V_FriendCounts_Users
AS
SELECT TOP 10 T.userId, firstName, lastName, friendCount FROM (
	SELECT firstUserId AS userId, Count(*) AS friendCount FROM socnet.Friendships
	WHERE level = 0
	GROUP BY firstUserId
) T
JOIN socnet.Users ON Users.id = T.userId

GO

CREATE VIEW socnet.V_Count_FriendRequests
AS
SELECT status, Count(*) AS requestCount FROM socnet.FriendRequests
GROUP BY FriendRequests.status

GO

CREATE VIEW socnet.V_FriendCountsRanges_Users
AS
SELECT T.countRange FROM (
	SELECT CASE
		WHEN friendCount < 10 THEN '<10'
		WHEN friendCount BETWEEN 10 AND 49 THEN '10-49'
		WHEN friendCount BETWEEN 50 AND 99 THEN '50-99'
		WHEN friendCount >= 100 THEN '>=100'
	END AS countRange FROM socnet.V_FriendCounts_Users
) T

GO

CREATE VIEW socnet.V_UsersCreatedRanges_MonthYear
AS
SELECT DATEPART(YEAR, registrationTime) AS Year, DATEPART(MONTH, registrationTime) AS Month, Count(*) AS userCount
FROM socnet.Users
GROUP BY DATEPART(MONTH, registrationTime), DATEPART(YEAR, registrationTime)

GO

CREATE VIEW socnet.V_Top10EditCount_User
AS
SELECT TOP 10 T.editorId, firstName, lastName, T.editCount FROM (
	SELECT editorId, Count(*) AS editCount FROM socnet.Edits
	GROUP BY editorId
) T
JOIN socnet.Users ON Users.id = T.editorId
ORDER BY editCount DESC

GO

-- average time to comments since posting in seconds
CREATE VIEW socnet.V_Top10AvgCommentTime_Posts
AS
SELECT Avg(DATEDIFF(SECOND, Elements.creationTime, PostElement.creationTime)) AS avgCommentTimeSeconds FROM socnet.Comments
JOIN socnet.Elements PostElement ON PostElement.id = Comments.postId
JOIN socnet.Elements ON Elements.id = Comments.id
GROUP BY postId